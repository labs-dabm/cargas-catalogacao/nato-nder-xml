from PySide6 import QtCore
from PySide6.QtCore import QCoreApplication
from PySide6.QtGui import QIcon
import os
from PySide6.QtWidgets import(QApplication, QFileDialog,
                              QMainWindow, QMessageBox, QTreeWidgetItem,
                              QWidget)
import sys
# https://matiascodesal.com/blog/spice-your-qt-python-font-awesome-icons/
import qtawesome as qta
from resources.login import Ui_Login
from resources.principal import Ui_MainWindow
from models.NcageXMLRead import NcageXMLRead
from data.database_sqlite import DatabaseSQLite


class Login(QWidget, Ui_Login):
    def __init__(self) -> None:
        super(Login, self).__init__()
        self.tentativas = 0
        self.setupUi(self)
        self.setWindowTitle("Login do Sistema")
        appIcon = QIcon('src/resources/img/dabm.png')
        self.setWindowIcon(appIcon)
        icon = qta.icon('mdi.login-variant', color="#fd0")
        self.btn_login.setIcon(icon)

        self.btn_login.clicked.connect(self.checkLogin)

    def open_system(self):
        user = self.input_user.text()
        pwd = self.input_senha.text()

        # if user == 'admin' and pwd == 'admin':
        #     self.w = MainWindow(user.upper(), pwd)
        #     self.w.show()
        #     self.close()
        # else:
        #     print('senha/usuário inválido')

        self.w = MainWindow(user.upper(), pwd)
        self.w.show()
        self.close()

    def checkLogin(self):

        user = self.input_user.text()
        pwd = self.input_senha.text()
        self.users = DatabaseSQLite()
        self.users.conecta()
        autenticado = self.users.check_user(user.upper(), pwd)

        if autenticado.lower() == "administrador" or autenticado.lower() == "user":

            self.w = MainWindow(user, autenticado.lower())
            self.w.show()
            self.close()
        else:

            if self.tentativas < 3:
                msg = QMessageBox()
                msg.setIcon(QMessageBox.Warning)
                msg.setWindowTitle("Erro ao acessar")
                msg.setText(f'Login ou senha incorreto \n \n Tentativa: {self.tentativas +1} de 3')
                msg.exec_()
                self.tentativas += 1
            if self.tentativas == 3:
                # bloquear o usuário
                self.users.close_connection()
                sys.exit(0)

        # self.w = MainWindow(user.upper(), pwd)
        # self.w.show()
        # self.close()


class MainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self, username, user):
        super(MainWindow, self).__init__()
        self.setupUi(self)
        self.setWindowTitle("NMCRL - Carga de Dados")
        appIcon = QIcon('src/resources/img/dabm.png')
        self.setWindowIcon(appIcon)

        self.user = username
        if user.lower() == "user":
            self.btn_pg_cadastro.setVisible(False)

        self.setIcones()

        # *************PAGINAS DO SISTEMA*************************************#
        self.Pages.setCurrentWidget(self.pg_home)
        self.btn_home.clicked.connect(lambda: self.Pages.setCurrentWidget(self.pg_home))
        self.btn_ncage.clicked.connect(lambda: self.Pages.setCurrentWidget(self.pg_table))
        self.btn_nsn.clicked.connect(lambda: self.Pages.setCurrentWidget(self.pg_nsn))
        self.btn_contato.clicked.connect(lambda: self.Pages.setCurrentWidget(self.pg_contato))
        self.btn_sobre.clicked.connect(lambda: self.Pages.setCurrentWidget(self.pg_sobre))
        self.btn_pg_cadastro.clicked.connect(lambda: self.Pages.setCurrentWidget(self.pg_cadastro))

        # ARQUIVO XML
        self.btn_abrir_dir.clicked.connect(self.open_path)
        self.pb_carga.setVisible(False)
        self.btn_importar.clicked.connect(self.import_xml_files)

        # chamando funcoes
        self.bt_cadastrar.clicked.connect(self.subscribe_user)

    def setIcones(self):

        iconHome = qta.icon('fa5s.home', color="#f8f9fa")
        self.btn_home.setIcon(iconHome)
        iconNcage = qta.icon('mdi.xml', color="#f8f9fa")
        self.btn_ncage.setIcon(iconNcage)
        self.btn_nsn.setIcon(iconNcage)
        iconContato = qta.icon('fa.envelope', color="#f8f9fa")
        self.btn_contato.setIcon(iconContato)
        iconSobre = qta.icon('fa.check', color="#f8f9fa")
        self.btn_sobre.setIcon(iconSobre)
        iconCarregar = qta.icon('fa.rotate-left', color="#f8f9fa")
        self.btn_carregar.setIcon(iconCarregar)
        iconSearch = qta.icon('mdi6.database-search', color="#f8f9fa")
        self.btn_pesquisar_ncage.setIcon(iconSearch)
        iconFolder = qta.icon('ei.folder-open', color="#f8f9fa")
        self.btn_abrir_dir.setIcon(iconFolder)
        iconImport = qta.icon('mdi6.database-import', color="#f8f9fa")
        self.btn_importar.setIcon(iconImport)
        iconStop = qta.icon('fa5.stop-circle', color="#f8f9fa")
        self.btn_parar.setIcon(iconStop)
        iconUser = qta.icon('fa.user-plus', color="#f8f9fa")
        self.bt_cadastrar.setIcon(iconUser)
        iconUserPlus = qta.icon('fa.user', color="#f8f9fa")
        self.btn_pg_cadastro.setIcon(iconUserPlus)

    def open_path(self):
        self.path = QFileDialog.getExistingDirectory(self, str("Open Directory"),
                                                     QtCore.QDir.homePath(),
                                                     QFileDialog.ShowDirsOnly | QFileDialog.DontResolveSymlinks
                                                     )
        self.path = QtCore.QDir.toNativeSeparators(self.path)
        self.input_file.setText(self.path)
        self.lw_arquivos.clear()
        all = [os.path.join(self.input_file.text(), arq)
               for arq in os.listdir(self.input_file.text())
               if arq.lower().endswith(".xml")]
        self.lw_arquivos.addItems(all)

    def import_xml_files(self):

        xml = NcageXMLRead(self.input_file.text())
        all = xml.all_files()
        self.pb_carga.setMaximum(len(all))
        self.pb_carga.setVisible(True)
        self.pb_carga.setValue(0)

        # db = DataBase()
        # db.conecta()
        cont = 1

        for i in all:
            self.pb_carga.setValue(cont)
            fullDataSet = xml.xml_data(i)
            # db.insert_data(fullDataSet)
            cont += 1

        # ATUALIZA A TABELA

        msg = QMessageBox()
        msg.setIcon(QMessageBox.Information)
        msg.setWindowTitle("Importação XML")
        msg.setText("importação concluída!")
        msg.exec_()
        self.pb_carga.setValue(0)

        # db.close_connection()

    def subscribe_user(self):

        if self.input_senha.text() != self.input_senha_2.text():
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Warning)
            msg.setWindowTitle("Senhas divirgentes")
            msg.setText("A senha não é igual!")
            msg.exec_()
            return None

        nome = self.input_nome.text()
        user = self.input_user.text()
        password = self.input_senha.text()
        access = self.cb_perfil.currentText()

        db = DatabaseSQLite()
        db.conecta()
        db.insert_user(nome, user, password, access)
        db.close_connection()

        msg = QMessageBox()
        msg.setIcon(QMessageBox.Information)
        msg.setWindowTitle("Cadastro de usuário")
        msg.setText("Cadastro realizado com sucesso!")
        msg.exec_()

        self.input_nome.setText("")
        self.input_user.setText("")
        self.input_senha.setText("")
        self.input_senha_2.setText("")


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = Login()
    window.show()
    # app.exec_()
    try:
        sys.exit(app.exec())
    except SystemExit:
        print('Closing Window...')
