class Ncage:
    """
     :19 TLX, :20 VTX, :21 PSS,
     :22 INF,  :23 KISC, :24 FOOTPRINT, :25 VERSION_ID, :26 KTIMESTAMP,
     :27 KPRMSR
    (
     :1 NCAGE, :2 KUSFDDC, :3 KNORG, :4 BOX, :5 OBEORG, :6 KSTUSCAN,
     :7 KSTJINY, :8 KPCC, :9 PSC, :10 KPCS, :11 KNSCMSD, :12 KTOENT, 
     :13 KIDN, :14 KDZMKHN, :15 KMOZ, :16 STREET1, :17 STREET2,
     :18 DUNS,
    ) """

    def __init__(self):
        self.ncage = None  # :1 NCAGE -> drn_4140
        self.drn_4235 = None  # :2 KUSFDDC->FOREIGN_DOMESTIC_DESIGNATOR_CODE_4235
        self.drn_8972 = None  # :3 KNORG->NCAGE_NAME_8972
        self.drn_1361 = None  # :4 BOX -> POST_OFFICE_BOX_1361
        self.drn_1084 = None  # :5 OBEORG -> GEO_ADDRESS_CITY_1084
        self.drn_0186 = None  # :6 KSTUSCAN->US_STATE_ABBREVIATION_0186
        self.drn_8978 = None  # :7 KSTJINY (STT) -> PROVINCE_NAME_8978
        self.drn_2659 = None  # :8 KPCC-> POSTAL_ADDRESS_CITY_2659
        self.drn_2549 = None  # :9 PSC (CEP) -> GEO_ADDRESS_POSTAL_ZONE_2549
        self.drn_2660 = None  # :10 KPCS -> POSTAL_ADDRESS_POSTAL_CODE_2660
        self.drn_2694 = None  # :11 KNSCMSD->NCAGE_STATUS_CODE_2694
        self.drn_4238 = None  # :12 KTOENT->NCAGE_TYPE_CODE_4238
        self.drn_2658 = None  # :13 KIDN->NATIONAL_IDENTIFICATION_NUMBER_2658
        self.drn_9567 = None  # :14 KDZMKHN->DATE_LAST_CHANGE_NCAGE_RECORD_9567
        self.drn_3408 = None  # :15 KMOZ(ISO KCZEME)->COUNTRY_CODE_3408
        self.drn_1082 = None  # :16 STREET1 -> STREET_ADDRESS_LINE_1_1082
        self.drn_1083 = None  # :17 STREET2 -> STREET_ADDRESS_LINE_2_1083
        self.drn_3405 = None  # :18 DUNS -> DUNS_CODE_3405
        self.drn_2262 = None  # :19 DRN_2262 -> DATE_NCAGE_ESTABLISHED_2262 -> no contain

        # :11 TLX, :12 VTX, :13 PSS, :14 INF,
        # :18 KISC
        # :21 FOOTPRINT, :22 VERSION_ID,
        # :25 KTIMESTAMP
        # :27 KPRMSR

        # omitido os ls_drn...


if __name__ == "__main__":

    p = Ncage()
    print(p.ncage)
    print(p.drn_9567)
