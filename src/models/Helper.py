from datetime import datetime, date


class Helper():

    @staticmethod
    def date_to_julian(date_time_str, full=True):
        """
        Convert to Julian date from datetime with 7 or 5 digits
        Argument 1: date_time_str = '19/05/20'
        Argument 2: full = True or False
        return str | None
        """
        # initializing format
        formato = '%d/%m/%y'
        # checking if format matches the date
        res = True
        # testa se veio vazio
        if date_time_str == '':
            date_time_obj = datetime.now()
        else:
            try:
                res = bool(datetime.strptime(date_time_str, formato))
            except ValueError:
                res = False
            # testa res se eh true
            if res:
                date_time_obj = datetime.strptime(
                    date_time_str, formato)
            else:
                # return "Incorrect data format, should be DD/MM/YY"
                return None

        # testa se o retorno eh full: 7 digitos ou nao: 5 digitos
        if full:
            return date_time_obj.strftime("%Y%j")
        else:
            return date_time_obj.strftime("%y%j")

    ########################################################################
    # https://www.geeksforgeeks.org/python-validate-string-date-format/
    # https://stackoverflow.com/questions/16870663/how-do-i-validate-a-date-string-format-in-python
    # http://www.longpelaexpertise.com.au/toolsJulian.php
    #
    @staticmethod
    def julian_to_date(juliana_str):
        """
        Convert Date from Julian with 7 or 5 digits
        Argument 1: juliana_str = '20140' or '2020140'
        return str | None
        """
        tam = len(juliana_str)
        res = True
        formato = ""
        if tam == 5:
            #
            formato = "%y%j"
        elif tam == 7:
            #
            formato = "%Y%j"
        else:
            # return "Incorrect data julian format"
            return ''

        try:
            res = bool(datetime.strptime(
                juliana_str, formato).strftime("%d/%m/%Y"))
        except ValueError:
            res = False

        if res:
            return datetime.strptime(juliana_str, formato).strftime("%d/%m/%Y")
        else:
            # return "Incorrect data julian format"
            return ''


if __name__ == "__main__":
    # SRC_DIR = os.path.join(os.path.abspath('.'), 'src')  # dir de codigo
    # SRC_DIR = os.path.dirname(os.path.abspath(__file__))  # dir de codigo
    # BASE_DIR = os.path.dirname(SRC_DIR)  # dir do projeto
    # converte uma data em formato juliana com 7 digitos
    print(f'Data to Juliana: {Helper.date_to_julian("19/05/20")}')
    # converte a data atual do sistema em formato juliana com 5 ou 7 digitos
    print(f'Data to Juliana: {Helper.date_to_julian("", False)}')
    #
    # converte uma data Juliana para date com 5 digitos
    print(f'Juliana to Data: {Helper.julian_to_date("22155")}')
    # converte uma data Juliana para date com 7 digitos
    print(f'Juliana to Data: {Helper.julian_to_date("2022155")}')
    # converte uma data Juliana
    print(f'Juliana to Data: {Helper.julian_to_date("")}')
