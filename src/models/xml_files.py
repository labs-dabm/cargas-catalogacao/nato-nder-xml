import os
import xml.etree.ElementTree as Et
from datetime import datetime, date


class ReadXml():
    def __init__(self, directory) -> None:
        self.directory = directory

    def all_files(self):
        return [os.path.join(self.directory, arq)
                for arq in os.listdir(self.directory)
                if arq.lower().endswith(".xml")]

    def xml_data(self, xml):
        root = Et.parse(xml).getroot()
        ns_ncs = {"ns": "http://eportal.nspa.nato.int/RawFileNcage/1.0"}
        # DADOS
        # 1
        total_files = self.check_none(
            root.find("./ns:HEADER/ns:TOTAL_FILES_NUMBER", ns_ncs))
        # 2
        total_xml = self.check_none(
            root.find("./ns:HEADER/ns:DATA_TYPE_IN_CURRENT_FILE_NUMBER",
                      ns_ncs))
        # 3
        dt_extracao = self.check_none(
            root.find("./ns:HEADER/ns:EXTRACTION_DATE_TIME", ns_ncs))

        data_importacao = date.today()
        data_importacao = data_importacao.strftime('%d/%m/%Y')
        # data_saida = ""
        # usuario = ""
        item = 1
        ncages = []

        print(f'Total Files: {total_files}')
        print(f'Total XML: {total_xml}')
        print(f'Data extracao: {dt_extracao}')

        for item in root.findall("./ns:BODY", ns_ncs):
            # DADOS DO ITEM ========================
            ncage = self.check_none(
                item.find(".ns:ncage/ns:NCAGE_CODE_4140", ns_ncs))

            data = self.check_none(
                item.find(".ns:ncage/ns:DATE_NCAGE_ESTABLISHED_2262", ns_ncs))

            dados = [ncage, data]

            ncages.append(dados)
            item += 1

        return ncages

    def check_none(self, var):
        if var is None:
            return ""
        else:
            try:
                return var.text
            except Exception:
                return var.text

    @staticmethod
    def julian_to_date(juliana_str):
        """
        Convert Date from Julian with 7 or 5 digits

        Argument 1: juliana_str = '20140' or '2020140'

        return str | None
        """
        tam = len(juliana_str)
        res = True
        format = ""
        if tam == 5:
            #
            format = "%y%j"
        elif tam == 7:
            #
            format = "%Y%j"
        else:
            # return "Incorrect data julian format"
            return None

        try:
            res = bool(datetime.strptime(
                juliana_str, format).strftime("%d/%m/%Y"))
        except ValueError:
            res = False

        if res:
            return datetime.strptime(juliana_str, format).strftime("%d/%m/%Y")
        else:
            # return "Incorrect data julian format"
            return None

    @staticmethod
    def date_to_julian(date_time_str, full=True):
        """
        Convert to Julian date from datetime with 7 or 5 digits

        Argument 1: date_time_str = '19/05/20'
        Argument 2: full = True or False

        return str | None
        """
        # initializing format
        format = '%d/%m/%y'
        # checking if format matches the date
        res = True
        # testa se veio vazio
        if date_time_str == '':
            date_time_obj = datetime.now()
        else:
            try:
                res = bool(datetime.strptime(date_time_str, format))
            except ValueError:
                res = False
            # testa res se eh true
            if res:
                date_time_obj = datetime.strptime(
                    date_time_str, format)
            else:
                # return "Incorrect data format, should be DD/MM/YY"
                return None

        # testa se o retorno eh full: 7 digitos ou nao: 5 digitos
        if full:
            return date_time_obj.strftime("%Y%j")
        else:
            return date_time_obj.strftime("%y%j")


if __name__ == "__main__":
    # print(__name__)
    # converte uma data em formato juliana com 5 digitos
    print(f'Data to Juliana: {ReadXml.date_to_julian("19/05/20", False)}')
    # converte uma data em formato juliana com 7 digitos
    print(f'Data to Juliana: {ReadXml.date_to_julian("19/05/20")}')
    # converte a data atual do sistema em formato juliana com 5 ou 7 digitos
    print(f'Data to Juliana: {ReadXml.date_to_julian("", False)}')

    # converte uma data Juliana para date com 5 digitos
    print(f'Juliana to Data: {ReadXml.julian_to_date("19155")}')
    # converte uma data Juliana para date com 7 digitos
    print(f'Juliana to Data: {ReadXml.julian_to_date("2022155")}')
    # converte uma data Juliana
    print(f'Juliana to Data: {ReadXml.julian_to_date("")}')

    # instanciando o objeto
    xml = ReadXml(
        'wiki/xml/extrato_ncage')
    # ler todos os arquivos
    all = xml.all_files()

    print(all)

    # for i in all:
    #     result = xml.xml_data(i)

    # print(result)
