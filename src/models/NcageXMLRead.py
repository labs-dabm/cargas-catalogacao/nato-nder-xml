import os
import xml.etree.ElementTree as Et
from datetime import datetime, date
import pandas as pd
# from ncage import Ncage
from src.models.Helper import Helper


class NcageXMLRead():
    def __init__(self, directory) -> None:
        self.directory = directory

    def all_files(self):
        return [os.path.join(self.directory, arq)
                for arq in os.listdir(self.directory)
                if arq.lower().endswith(".xml")]

    def read_header_xml(self) -> None:
        """Retorna o total de arquivos XML
           Total de Itens em cada XML
           Data da Extracao do RAW DATA
        """
        print(f'total_files: {self.total_files.text}')
        print(f'total_xml: {self.total_itens_xml.text}')
        print(f'dt_extracao: {self.data_extracao.text}')

    def xml_data(self, xml):
        root = Et.parse(xml).getroot()
        # print(root.tag, root.attrib)
        ns = {"ns": "http://eportal.nspa.nato.int/RawFileNcage/1.0"}

        # informacoes do cabecalho do XML
        self.total_files = root.find("./HEADER/TOTAL_FILES_NUMBER", ns)
        self.total_itens_xml = root.find("./HEADER/DATA_TYPE_IN_CURRENT_FILE_NUMBER", ns)
        self.data_extracao = root.find("./HEADER/EXTRACTION_DATE_TIME", ns)

        data_importacao = date.today()
        data_importacao = data_importacao.strftime('%d/%m/%Y')
        # data_saida = ""
        # usuario = ""
        i = 1
        all_ncages = []
        # atributos para armazenar as lista das tags do XML que
        # possuem mais de um registro
        # ex: telefones, faxes, websites, substituicoes
        self.ls_drn_8974 = []
        # self.ls_drn_8974_p1 = []
        self.ls_drn_8975 = []
        self.ls_drn_3375 = []
        self.ls_drn_8021 = []
        self.ls_drn_1368 = []
        self.ls_drn_6044 = []
        self.ls_drn_2657 = []
        self.ls_drn_9569 = []
        self.ls_drn_9574 = []
        self.ls_drn_9568 = []
        self.ls_drn_3595 = []

        # faz a varredura no corpo principal do XML
        for item in root.findall("./BODY/ncage", ns):
            """ Para cada registro (NCAGE) encontrado ele vai extrair
                as tags para montar a estrutura do SEG 8 XML 2022
            """

            # print(item)
            # tag principal NCAGE
            ncage = self.check_none(item.find("NCAGE_CODE_4140", ns))
            drn_2262 = Helper.julian_to_date(self.check_none(
                item.find("DATE_NCAGE_ESTABLISHED_2262", ns)))
            drn_9567 = Helper.julian_to_date(self.check_none(
                item.find("DATE_LAST_CHANGE_NCAGE_RECORD_9567", ns)))

            # tag ncagedata
            drn_8972 = self.check_none(
                item.find("ncagedata/NCAGE_NAME_8972", ns))
            drn_2694 = self.check_none(
                item.find("ncagedata/NCAGE_STATUS_CODE_2694", ns))
            drn_4235 = self.check_none(
                item.find("ncagedata/FOREIGN_DOMESTIC_DESIGNATOR_CODE_4235", ns))
            drn_4238 = self.check_none(
                item.find("ncagedata/NCAGE_TYPE_CODE_4238", ns))
            drn_3408 = self.check_none(
                item.find("ncagedata/COUNTRY_CODE_3408", ns))

            # tag ncagedata/state opcional
            drn_0186 = self.check_none(
                item.find("ncagedata/state/US_STATE_ABBREVIATION_0186", ns))
            drn_8978 = self.check_none(
                item.find("ncagedata/state/PROVINCE_NAME_8978", ns))

            # tag ncagedata/physical_address opcional
            drn_1082 = self.check_none(
                item.find("ncagedata/physical_address/STREET_ADDRESS_LINE_1_1082", ns))
            drn_1083 = self.check_none(
                item.find("ncagedata/physical_address/STREET_ADDRESS_LINE_2_1083", ns))
            drn_2549 = self.check_none(
                item.find("ncagedata/physical_address/GEO_ADDRESS_POSTAL_ZONE_2549", ns))
            drn_1084 = self.check_none(
                item.find("ncagedata/physical_address/GEO_ADDRESS_CITY_1084", ns))

            # tag ncagedata/postal_address opcional
            drn_1361 = self.check_none(
                item.find("ncagedata/postal_address/POST_OFFICE_BOX_1361", ns))
            drn_2660 = self.check_none(
                item.find("ncagedata/postal_address/POSTAL_ADDRESS_POSTAL_CODE_2660", ns))
            drn_2659 = self.check_none(
                item.find("ncagedata/postal_address/POSTAL_ADDRESS_CITY_2659", ns))

            # tag ncagedata/communication
            # tag ncagedata/communication/telephones min1 max 5
            drn_8974 = (item.findall(
                "ncagedata/communication/telephones/TELEPHONE_NUMBER_8974", ns))
            ls_drn_8974 = self.busca_aninhada_tupla(drn_8974)
            # ls_drn_8974_p1 = self.busca_aninhada_tupla_tupla(drn_8974)
            # tag ncagedata/communication/faxes min1 max 5
            drn_8975 = (item.findall(
                "ncagedata/communication/faxes/FAX_NUMBER_8975", ns))
            ls_drn_8975 = self.busca_aninhada_tupla(drn_8975)
            # ls_drn_8975 = drn_8975
            # tag ncagedata/communication/emails min1 max 5
            drn_3375 = (item.findall(
                "ncagedata/communication/emails/EMAIL_ADDRESS_3375", ns))
            ls_drn_3375 = self.busca_aninhada_tupla(drn_3375)
            # tag ncagedata/communication/websites min1 max 5
            drn_8021 = item.findall(
                "ncagedata/communication/websites/WEB_URL_8021", ns)
            ls_drn_8021 = self.busca_aninhada_tupla(drn_8021)

            # tag ncagedata/additional/
            drn_2658 = self.check_none(
                item.find("ncagedata/additional/NATIONAL_IDENTIFICATION_NUMBER_2658", ns))
            # tag ncagedata/additional/sicc_codes - min. 0, max. 15
            drn_1368 = (
                item.findall("ncagedata/additional/sicc_codes/STANDARD_INDUSTRIAL_CLASSIFICATION_CODE_1368", ns))
            ls_drn_1368 = self.busca_aninhada_tupla(drn_1368)

            # tag ncagedata/additional/naics_codes - min. 0, max. 15
            drn_6044 = item.findall(
                "ncagedata/additional/naics_codes/NAICS_CODE_6044", ns)
            ls_drn_6044 = self.busca_aninhada_tupla(drn_6044)

            # tag ncagedata/additional/nace_codes - min. 0, max. 15
            drn_2657 = item.findall(
                "ncagedata/additional/nace_codes/NACE_CODE_2657", ns)
            ls_drn_2657 = self.busca_aninhada_tupla(drn_2657)

            # tag ncagedata/additional/
            drn_3405 = self.check_none(
                item.find("ncagedata/additional/DUNS_CODE_3405", ns))

            # tag ncagedata/additional/CPV_codes - min. 0, max. 15
            drn_9569 = item.findall(
                "ncagedata/additional/CPV_codes/CPV_CODE_9569", ns)
            ls_drn_9569 = self.busca_aninhada_tupla(drn_9569)

            # tag ncagedata/additional/UNSPSC_codes - min. 0, max. 15
            drn_9574 = item.findall(
                "ncagedata/additional/UNSPSC_codes/UNSPSC_CODE_9574", ns)
            ls_drn_9574 = self.busca_aninhada_tupla(drn_9574)

            # tag ncagedata/additional/GLN_codes - min. 0, max. 15
            drn_9568 = item.findall(
                "ncagedata/additional/GLN_codes/GLN_CODE_9568", ns)
            ls_drn_9568 = self.busca_aninhada_tupla(drn_9568)

            # tag ncagedata/additional/replacements - min. 1, max. 5
            drn_3595 = item.findall(
                "ncagedata/additional/replacements/REPLACEMENT_NCAGE_3595", ns)
            ls_drn_3595 = self.busca_aninhada_tupla(drn_3595)

            # mais performatico
            tupla = (ncage, drn_4235, drn_8972, drn_1361, drn_1084,
                     drn_0186, drn_8978, drn_2659, drn_2549, drn_2660,
                     drn_2694, drn_4238, drn_2658, drn_9567, drn_3408,
                     drn_1082, drn_1083, drn_3405, drn_2262)

            # lista = [ncage, ls_drn_8974, ls_drn_8975, ls_drn_3375, ls_drn_8021,
            #          ls_drn_1368, ls_drn_6044, ls_drn_2657, ls_drn_9569,
            #          ls_drn_9574, ls_drn_9568, ls_drn_3595]

            # cria as lista com as tags que possuem + de um registro
            self.ls_drn_8974.append((ncage, ls_drn_8974))
            # self.ls_drn_8974_p1.append((ncage, ls_drn_8974_p1))

            self.ls_drn_8975.append((ncage, ls_drn_8975))
            self.ls_drn_3375.append((ncage, ls_drn_3375))
            self.ls_drn_8021.append((ncage, ls_drn_8021))
            self.ls_drn_1368.append((ncage, ls_drn_1368))
            self.ls_drn_6044.append((ncage, ls_drn_6044))
            self.ls_drn_2657.append((ncage, ls_drn_2657))
            self.ls_drn_9569.append((ncage, ls_drn_9569))
            self.ls_drn_9574.append((ncage, ls_drn_9574))
            self.ls_drn_9568.append((ncage, ls_drn_9568))
            self.ls_drn_3595.append((ncage, ls_drn_3595))

            all_ncages.append(tupla)
            # all_ncages.append(lista)
            i += 1
        return all_ncages

    def busca_aninhada(self, drn):
        """ Percorre a lista e retorna tudo em um linha com o
            separador #
        """
        retorno = ''
        if not (drn is None):
            if len(drn) > 0:
                for d in drn:
                    retorno += self.check_none(d) + "#"
                retorno = retorno[:-1]
        return retorno

    def busca_aninhada_tupla(self, drn):
        """ Percorre a lista e retorna tudo em uma tupla
            contendo cada registro encontrado, sena retorna vazio
        """
        retorno = []
        if not (drn is None):
            if len(drn) > 0:
                for d in drn:
                    retorno.append(self.check_none(d))
        return tuple(retorno)

    @ staticmethod
    def monta_tupla_ncage(tupla):
        # resposta = []
        resposta = set()
        if len(tupla) > 0:
            for ncage, dados in tupla:
                # print(ncage, dados)
                if len(dados) > 0:
                    for d in dados:
                        # resposta.append((ncage, d))
                        resposta.add((ncage, d))

        # return resposta
        return list(resposta)

    @ staticmethod
    def check_none(var):
        if var is None:
            return ''
        else:
            try:
                return var.text
            except Exception:
                return ''

    @ staticmethod
    def check_non1e(var):
        if var is None:
            return None
        else:
            try:
                return var.text
            except Exception:
                return None


if __name__ == "__main__":
    # SRC_DIR = os.path.join(os.path.abspath('.'), 'src')  # dir de codigo
    # SRC_DIR = os.path.dirname(os.path.abspath(__file__))  # dir de codigo
    # BASE_DIR = os.path.dirname(SRC_DIR)  # dir do projeto

    # instanciando o objeto
    # path_ncage = 'D:/SERVCAT/02.Novo-Raw-Data-NMRCL-2022/ncage'
    # path_ncage = 'D:\\projetos\\data_science\\2022\\projeto-nadex-nder\\wiki\\xml\\extrato_ncage'
    # pycharm
    # xml = NcageXMLRead('../../wiki/xml/extrato_ncage')
    # xml = NcageXMLRead('../../wiki/xml/ncage')
    # vscode code runner
    xml = NcageXMLRead('wiki/xml/extrato_ncage')
    # ler todos os arquivos
    lista_xml = xml.all_files()
    # print(lista_xml)
    #######################
    # # funcionou com o pandas no console - Anders
    # all_items = xml.xml_data(lista_xml[0])
    # # print(all_items)
    # df = pd.DataFrame(all_items,
    #                   columns=['NCAGE', 'Date', 'Date Last Change',
    #                            'NCAGE Name', 'Status Code', 'Designator Code',
    #                            'Type Code', 'Country Code', 'US State',
    #                            'Province Name', 'Street 1', 'Street 2',
    #                            'Postal Zone', 'Geographical',
    #                            'Post Office', 'Postal Code',
    #                            'Postal Address', 'Telephones', 'Faxes',
    #                            'Emails', 'Websites', 'National Identification',
    #                            'ISIC', 'NAICS', 'NACE', 'DUNS', 'CPV',
    #                            'UNSPSC', 'GLN', 'Replacement'
    #                            ])
    # print(df.head(100))
    # print(df)
    #######################
    # mais performatico no console
    for i in lista_xml:
        result = xml.xml_data(i)

        # print(result)

    # print(len(lista_xml))
    all_items = xml.xml_data(lista_xml[0])
    print((all_items))
