import cx_Oracle
from os import environ
from os.path import join, dirname, abspath
from dotenv import load_dotenv

# https://cx-oracle.readthedocs.io/en/latest/user_guide/tracing_sql.html


class Connection(cx_Oracle.Connection):
    log_file_name = "log.txt"

    def __init__(self):
        SRC_DIR = join(abspath('.'), 'src')  # dir de codigo
        BASE_DIR = dirname(SRC_DIR)  # dir do projeto
        dotenv_path = join(BASE_DIR, '.env')
        load_dotenv(dotenv_path)

        self.usuario = environ.get("ORA_USERNAME")
        self.senha = environ.get("ORA_PASSWORD")
        self.ora_dsn = environ.get("ORA_DSN")
        self.ora_encoding = environ.get("ORA_ENCODING")

        # connect_string = "hr/hr_password@dbhost.example.com/orclpdb1"
        # connect_string = "fedlogdb/marinha@CATALOP1.MAR.MIL.BR"

        self._log("Connect to the database")
        self._conn = super(Connection, self).__init__(user=self.usuario,
                                                      password=self.senha,
                                                      # tnsname.ora
                                                      dsn=self.ora_dsn,
                                                      # jdbc
                                                      # dsn="10.1.2.241:1521/portalp1.mar.mil.br",
                                                      encoding=self.ora_encoding)

        return self._conn

    def _log(self, message):
        with open(self.log_file_name, "a") as f:
            print(message, file=f)

    def execute(self, sql, parameters):
        self._log(sql)
        cursor = self.cursor()
        try:
            return cursor.execute(sql, parameters)
        except cx_Oracle.Error as e:
            error_obj, = e.args
            self._log(error_obj.message)
            raise


if __name__ == "__main__":

    db = Connection()
    print('A versão do Oracle é: ', db.version)
    # db.execute("""
    #     select KHN.NCAGE
    #     from FEDLOGDB.NMCRL_ORG KHN
    #     where KHN.NCAGE = :ncage""", dict(ncage='00002'))
