import cx_Oracle as oracledb
from os import environ
from os.path import join, dirname, abspath
from dotenv import load_dotenv

# https://cx-oracle.readthedocs.io/en/latest/user_guide/connection_handling.html
# jdbc:oracle:thin:@hostname:port/service_name
# jdbc:oracle:thin:@10.1.2.241:1521/CATALOP1.MAR.MIL.BR

# via tnsname.ora configurado no path
# sqlplus mccprod/mccprod2021@MCCPROD.DABM.COM.BR

# via conexao direta como jdbc
# sqlplus mccprod/mccprod2021@10.1.2.241:1521/CATALOP1.MAR.MIL.BR

# ANDERS_ORACLE:marinha@10.1.2.241/?service_name=portalp1.mar.mil.br

# https://www.oracletutorial.com/python-oracle/connecting-to-oracle-database-in-python/
# https://cx-oracle.readthedocs.io/en/latest/user_guide/batch_statement.html


class DatabaseOracle():
    log_file_name = "log.txt"

    def __init__(self) -> None:
        SRC_DIR = join(abspath('.'), 'src')  # dir de codigo
        BASE_DIR = dirname(SRC_DIR)  # dir do projeto
        dotenv_path = join(BASE_DIR, '.env')
        load_dotenv(dotenv_path)

        ORA_USERNAME = environ.get("ORA_USERNAME")
        ORA_DSN = environ.get("ORA_DSN")
        ORA_ENCODING = environ.get("ORA_ENCODING")
        ORA_PASSWORD = environ.get("ORA_PASSWORD")

        self.user = ORA_USERNAME
        self.password = ORA_PASSWORD
        self.dsn = ORA_DSN
        self.encoding = ORA_ENCODING

    def connection(self):
        """
        Create connection com Oracle
        """
        try:
            self.conn = oracledb.connect(user=self.user,
                                         password=self.password,
                                         # tnsname.ora
                                         dsn=self.dsn,
                                         # jdbc
                                         # dsn="10.1.2.241:1521/portalp1.mar.mil.br",
                                         encoding=self.encoding)
        except oracledb.Error as err:
            print('Error while creating the connection: ', err)

    def version(self):
        try:
            self.connection()
            with self.conn as conn:
                print('A versão do Oracle é: ', conn.version)
        except AttributeError as err:
            err
            # print("Erro no metodo version(): ", err)

    def insert_ncage_dados(self, data, tb_column, limite=10000):
        """(
            :1 NCAGE, :2 DADO,
           )
            NMCRL_ORG_CPV
            NMCRL_ORG_FAX
            NMCRL_ORG_GLN
            NMCRL_ORG_MAIL
            NMCRL_ORG_NACE
            NMCRL_ORG_NAICS
            NMCRL_ORG_REPL
            NMCRL_ORG_SICC
            NMCRL_ORG_TEL
            NMCRL_ORG_UNSPSC
            NMCRL_ORG_WEB

        """
        param2 = tb_column
        if tb_column == 'REPL':
            param2 = 'REPLACEMENT'
        # Adjust the number of rows to be inserted in each iteration
        # to meet your memory and performance requirements
        # https://cx-oracle.readthedocs.io/en/latest/user_guide/batch_statement.html
        batch_size = 10000
        try:
            self.connection()
            with self.conn as conn:
                # create a cursor
                with conn.cursor() as cursor:
                    # execute the insert statement
                    cursor = self.conn.cursor()
                    sql_insert = """ INSERT INTO FEDLOGDB.NMCRL_ORG_%s
                    (NCAGE, %s)
                    values
                    (:1,:2)
                    """ % (tb_column, param2)
                    # commit work
                    if len(data) % batch_size == 0:
                        cursor.executemany(sql_insert, data)
                        data = []

                    if data:
                        cursor.executemany(sql_insert, data)

                    conn.commit()
        except Exception as err:
            print('Erro durante a inserção do dados: ', err)

    def insert_ncage_org(self, data):
        """(
            :1 NCAGE, :2 KUSFDDC, :3 KNORG, :4 BOX, :5 OBEORG, :6 KSTUSCAN,
            :7 KSTJINY, :8 KPCC, :9 PSC, :10 KPCS, :11 KNSCMSD, :12 KTOENT,
            :13 KIDN, :14 KDZMKHN, :15 KMOZ, :16 STREET1, :17 STREET2,
            :18 DUNS,
            )
            DE FORA:
            :19 TLX, telex (type: String), DRN: A454 TLX
            :20 VTX, videotext (type: String), DRN: A455
            :21 PSS,  Address for handing over data files DRN: A453
            :22 INF, Supplementary information -internalNationalInfo, DRN: A433
            :23 KISC,
            :24 FOOTPRINT, footprint (type: Integer), DRN: No Drn
            :25 VERSION_ID,
            :26 KTIMESTAMP,
            :27 KPRMSR changeForMsr (type: boolean), DRN: No Drn
        """

        # Adjust the number of rows to be inserted in each iteration
        # to meet your memory and performance requirements
        batch_size = 10000
        try:
            self.connection()
            with self.conn as conn:
                # create a cursor
                with conn.cursor() as cursor:
                    # execute the insert statement
                    cursor = self.conn.cursor()
                    sql_insert = """ INSERT INTO FEDLOGDB.NMCRL_ORG
                    (NCAGE, KUSFDDC, KNORG, BOX, OBEORG, KSTUSCAN, KSTJINY,
                    KPCC, PSC, KPCS, KNSCMSD, KTOENT, KIDN, KDZMKHN, KMOZ,
                    STREET1, STREET2, DUNS, DRN_2262)
                    values
                    (:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,
                    TO_DATE(:14, 'DD/MM/YYYY'),
                    :15,:16,
                    :17,:18,TO_DATE(:19, 'DD/MM/YYYY'))
                    """

                    if len(data) % batch_size == 0:
                        cursor.executemany(sql_insert, data)
                        data = []

                    if data:
                        cursor.executemany(sql_insert, data)

                    conn.commit()
                    print('Dados inserido com sucesso!')
        except Exception as err:
            print('Erro durante a inserção do dados: ', err)

    def execute(self, sql):
        self._log(sql)
        self.connection()
        try:
            with self.conn as conn:
                # create a cursor
                with conn.cursor() as cursor:
                    cursor = self.conn.cursor()
                    cursor.execute(sql)
        except oracledb.Error as e:
            error_obj, = e.args
            self._log(error_obj.message)
            raise

    def _log(self, message):
        with open(self.log_file_name, "a") as f:
            print(message, file=f)


if __name__ == "__main__":

    db = DatabaseOracle()
    db.version()
