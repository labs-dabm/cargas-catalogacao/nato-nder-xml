# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'login.ui'
##
## Created by: Qt User Interface Compiler version 6.3.0
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QFrame, QLabel, QLineEdit,
    QPushButton, QSizePolicy, QWidget)

class Ui_Login(object):
    def setupUi(self, Login):
        if not Login.objectName():
            Login.setObjectName(u"Login")
        Login.resize(423, 378)
        Login.setStyleSheet(u"background-color: rgb(0, 65, 98); \n"
"/* background-color: #3196DE*/")
        self.frame = QFrame(Login)
        self.frame.setObjectName(u"frame")
        self.frame.setGeometry(QRect(70, 130, 281, 181))
        self.frame.setStyleSheet(u"QFrame{    \n"
"   background-color: rgba(0, 0, 0,0.2);\n"
"	border-radius:20px;\n"
"    border-radius: 8px;\n"
"}\n"
"QLineEdit{\n"
"	border-radius:3px;    \n"
"	background-color: rgb(255, 255, 255);   \n"
"	color: rgb(0, 0, 0);\n"
"}")
        self.frame.setFrameShape(QFrame.StyledPanel)
        self.frame.setFrameShadow(QFrame.Raised)
        self.input_user = QLineEdit(self.frame)
        self.input_user.setObjectName(u"input_user")
        self.input_user.setGeometry(QRect(40, 20, 205, 30))
        font = QFont()
        font.setPointSize(12)
        self.input_user.setFont(font)
        self.input_user.setStyleSheet(u"")
        self.input_user.setAlignment(Qt.AlignCenter)
        self.input_senha = QLineEdit(self.frame)
        self.input_senha.setObjectName(u"input_senha")
        self.input_senha.setGeometry(QRect(40, 60, 205, 30))
        self.input_senha.setFont(font)
        self.input_senha.setStyleSheet(u"")
        self.input_senha.setEchoMode(QLineEdit.Password)
        self.input_senha.setAlignment(Qt.AlignCenter)
        self.btn_login = QPushButton(self.frame)
        self.btn_login.setObjectName(u"btn_login")
        self.btn_login.setGeometry(QRect(40, 140, 210, 31))
        self.btn_login.setFont(font)
        self.btn_login.setCursor(QCursor(Qt.PointingHandCursor))
        self.btn_login.setStyleSheet(u"QPushButton{\n"
"	\n"
"	background-color: #004d71;\n"
"	color: rgb(255, 255, 255);\n"
"	border-radius:5px;\n"
"}\n"
"\n"
"QPushButton:hover{\n"
"	\n"
"	background-color: rgb(0, 0, 0);	\n"
"	color: rgb(255, 255, 255)\n"
"	\n"
"}")
        self.label = QLabel(Login)
        self.label.setObjectName(u"label")
        self.label.setGeometry(QRect(180, 40, 81, 81))
        self.label.setPixmap(QPixmap(u"img/User.png"))
        self.label.setScaledContents(False)

        self.retranslateUi(Login)

        QMetaObject.connectSlotsByName(Login)
    # setupUi

    def retranslateUi(self, Login):
        Login.setWindowTitle(QCoreApplication.translate("Login", u"Form", None))
        self.input_user.setPlaceholderText(QCoreApplication.translate("Login", u"Usu\u00e1rio", None))
        self.input_senha.setPlaceholderText(QCoreApplication.translate("Login", u"Senha", None))
        self.btn_login.setText(QCoreApplication.translate("Login", u"Login", None))
        self.label.setText("")
    # retranslateUi

