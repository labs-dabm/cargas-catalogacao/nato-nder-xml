# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'principal.ui'
##
## Created by: Qt User Interface Compiler version 6.3.0
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QComboBox, QFrame, QGridLayout,
    QHBoxLayout, QHeaderView, QLabel, QLineEdit,
    QListWidget, QListWidgetItem, QMainWindow, QProgressBar,
    QPushButton, QSizePolicy, QStackedWidget, QTabWidget,
    QTreeWidget, QTreeWidgetItem, QVBoxLayout, QWidget)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(800, 600)
        MainWindow.setStyleSheet(u"background-color: rgb(248, 249, 250);")
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.verticalLayout_2 = QVBoxLayout(self.centralwidget)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.frame = QFrame(self.centralwidget)
        self.frame.setObjectName(u"frame")
        self.frame.setCursor(QCursor(Qt.ArrowCursor))
        self.frame.setStyleSheet(u"QPushButton{\n"
"	background-color:#343a40;\n"
"    border-color: #6c757d;\n"
"    border-radius:None;\n"
"	color: #f8f9fa;\n"
"	font: 11pt \"Segoe UI\";\n"
"}\n"
"\n"
"QPushButton:hover{\n"
"	border-color: #204d74;\n"
"	background-color: #6c757d;\n"
"}\n"
"")
        self.frame.setFrameShape(QFrame.StyledPanel)
        self.frame.setFrameShadow(QFrame.Raised)
        self.frame.setLineWidth(1)
        self.horizontalLayout = QHBoxLayout(self.frame)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.btn_home = QPushButton(self.frame)
        self.btn_home.setObjectName(u"btn_home")
        self.btn_home.setMinimumSize(QSize(0, 35))
        self.btn_home.setCursor(QCursor(Qt.PointingHandCursor))
        self.btn_home.setStyleSheet(u"")

        self.horizontalLayout.addWidget(self.btn_home)

        self.btn_ncage = QPushButton(self.frame)
        self.btn_ncage.setObjectName(u"btn_ncage")
        self.btn_ncage.setMinimumSize(QSize(0, 35))
        self.btn_ncage.setCursor(QCursor(Qt.PointingHandCursor))

        self.horizontalLayout.addWidget(self.btn_ncage)

        self.btn_nsn = QPushButton(self.frame)
        self.btn_nsn.setObjectName(u"btn_nsn")
        self.btn_nsn.setMinimumSize(QSize(0, 35))
        self.btn_nsn.setCursor(QCursor(Qt.PointingHandCursor))

        self.horizontalLayout.addWidget(self.btn_nsn)

        self.btn_contato = QPushButton(self.frame)
        self.btn_contato.setObjectName(u"btn_contato")
        self.btn_contato.setMinimumSize(QSize(0, 35))
        self.btn_contato.setCursor(QCursor(Qt.PointingHandCursor))

        self.horizontalLayout.addWidget(self.btn_contato)

        self.btn_sobre = QPushButton(self.frame)
        self.btn_sobre.setObjectName(u"btn_sobre")
        self.btn_sobre.setMinimumSize(QSize(0, 35))
        self.btn_sobre.setCursor(QCursor(Qt.PointingHandCursor))

        self.horizontalLayout.addWidget(self.btn_sobre)


        self.verticalLayout_2.addWidget(self.frame)

        self.Pages = QStackedWidget(self.centralwidget)
        self.Pages.setObjectName(u"Pages")
        self.Pages.setStyleSheet(u"")
        self.pg_table = QWidget()
        self.pg_table.setObjectName(u"pg_table")
        self.gridLayout = QGridLayout(self.pg_table)
        self.gridLayout.setObjectName(u"gridLayout")
        self.tab_base = QTabWidget(self.pg_table)
        self.tab_base.setObjectName(u"tab_base")
        self.tab_ncage = QWidget()
        self.tab_ncage.setObjectName(u"tab_ncage")
        self.verticalLayout_3 = QVBoxLayout(self.tab_ncage)
        self.verticalLayout_3.setObjectName(u"verticalLayout_3")
        self.frame_2 = QFrame(self.tab_ncage)
        self.frame_2.setObjectName(u"frame_2")
        self.frame_2.setStyleSheet(u"QLineEdit{	\n"
"	color: rgb(52, 58, 64);\n"
"	border-bottom: 1px solid #343a40;\n"
"    border-radius: None;   \n"
"	background-color: rgba(85, 115, 155,0.1);\n"
"	font: 14px \"Trebuchet MS\";\n"
"}")
        self.frame_2.setFrameShape(QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_2 = QHBoxLayout(self.frame_2)
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.label_2 = QLabel(self.frame_2)
        self.label_2.setObjectName(u"label_2")
        self.label_2.setMinimumSize(QSize(130, 30))
        self.label_2.setMaximumSize(QSize(130, 30))
        font = QFont()
        font.setPointSize(10)
        self.label_2.setFont(font)

        self.horizontalLayout_2.addWidget(self.label_2)

        self.input_filtrar_ncage = QLineEdit(self.frame_2)
        self.input_filtrar_ncage.setObjectName(u"input_filtrar_ncage")
        self.input_filtrar_ncage.setMinimumSize(QSize(0, 30))

        self.horizontalLayout_2.addWidget(self.input_filtrar_ncage)

        self.line = QFrame(self.frame_2)
        self.line.setObjectName(u"line")
        self.line.setFrameShape(QFrame.VLine)
        self.line.setFrameShadow(QFrame.Sunken)

        self.horizontalLayout_2.addWidget(self.line)

        self.input_buscar_ncage = QLineEdit(self.frame_2)
        self.input_buscar_ncage.setObjectName(u"input_buscar_ncage")
        self.input_buscar_ncage.setMinimumSize(QSize(0, 30))

        self.horizontalLayout_2.addWidget(self.input_buscar_ncage)

        self.btn_pesquisar_ncage = QPushButton(self.frame_2)
        self.btn_pesquisar_ncage.setObjectName(u"btn_pesquisar_ncage")
        self.btn_pesquisar_ncage.setMinimumSize(QSize(110, 35))
        self.btn_pesquisar_ncage.setMaximumSize(QSize(16777215, 35))
        font1 = QFont()
        font1.setFamilies([u"Segoe UI"])
        font1.setPointSize(11)
        font1.setBold(False)
        font1.setItalic(False)
        self.btn_pesquisar_ncage.setFont(font1)
        self.btn_pesquisar_ncage.setStyleSheet(u"QPushButton{\n"
"	\n"
"border-color: #6c757d;\n"
"	background-color: #343a40;\n"
"    border-radius:None;\n"
"	color: #f8f9fa;\n"
"	font: 11pt \"Segoe UI\";\n"
"}\n"
"\n"
"QPushButton:hover{\n"
"	background-color:#6c757d;\n"
"    border-color: #343a40;\n"
"}")

        self.horizontalLayout_2.addWidget(self.btn_pesquisar_ncage)


        self.verticalLayout_3.addWidget(self.frame_2)

        self.horizontalLayout_6 = QHBoxLayout()
        self.horizontalLayout_6.setObjectName(u"horizontalLayout_6")
        self.btn_carregar = QPushButton(self.tab_ncage)
        self.btn_carregar.setObjectName(u"btn_carregar")
        self.btn_carregar.setMinimumSize(QSize(130, 35))
        self.btn_carregar.setMaximumSize(QSize(130, 35))
        self.btn_carregar.setStyleSheet(u"QPushButton{\n"
"	border-color: #204d74;\n"
"	background-color: #286090;	\n"
"    border-radius:None;\n"
"	color: #f8f9fa;\n"
"	font: 11pt \"Segoe UI\";\n"
"}\n"
"\n"
"QPushButton:hover{\n"
"\n"
"background-color:#337ab7;\n"
"    border-color: #2e6da4;\n"
"}")

        self.horizontalLayout_6.addWidget(self.btn_carregar)

        self.label_7 = QLabel(self.tab_ncage)
        self.label_7.setObjectName(u"label_7")

        self.horizontalLayout_6.addWidget(self.label_7)

        self.cb_linhas_pagina = QComboBox(self.tab_ncage)
        self.cb_linhas_pagina.addItem("")
        self.cb_linhas_pagina.addItem("")
        self.cb_linhas_pagina.addItem("")
        self.cb_linhas_pagina.addItem("")
        self.cb_linhas_pagina.addItem("")
        self.cb_linhas_pagina.addItem("")
        self.cb_linhas_pagina.setObjectName(u"cb_linhas_pagina")
        self.cb_linhas_pagina.setMinimumSize(QSize(130, 0))
        self.cb_linhas_pagina.setMaximumSize(QSize(130, 30))
        self.cb_linhas_pagina.setFont(font)
        self.cb_linhas_pagina.setStyleSheet(u"")

        self.horizontalLayout_6.addWidget(self.cb_linhas_pagina)


        self.verticalLayout_3.addLayout(self.horizontalLayout_6)

        self.tw_ncage = QTreeWidget(self.tab_ncage)
        __qtreewidgetitem = QTreeWidgetItem()
        __qtreewidgetitem.setText(0, u"NCAGE");
        self.tw_ncage.setHeaderItem(__qtreewidgetitem)
        self.tw_ncage.setObjectName(u"tw_ncage")
        self.tw_ncage.setStyleSheet(u"QTreeWidget {\n"
"	color: rgb(52, 58, 64);\n"
"font: 14px \"Trebuchet MS\";\n"
"/*border: 0 0 0 0;*/\n"
"border: 1px solid #343a40;\n"
"border-top:0px;\n"
"background-color: rgb(255, 255, 255);\n"
"	selection-background-color:transparent;\n"
"}\n"
"\n"
"QTreeWidget QHeaderView::section {\n"
"   color: black;                               \n"
"   padding: 2px;                        \n"
"    height:25px;                                \n"
"    border:  1px solid #343a40;                 \n"
"    border-left:0px;                     \n"
"    border-right:0px;\n"
"    background: #f9f9f9; \n"
"}\n"
"\n"
"")

        self.verticalLayout_3.addWidget(self.tw_ncage)

        self.tab_base.addTab(self.tab_ncage, "")
        self.tab_import = QWidget()
        self.tab_import.setObjectName(u"tab_import")
        self.tab_import.setStyleSheet(u"QLineEdit{\n"
"	\n"
"	color: rgb(52, 58, 64);\n"
"	border-bottom: 1px solid #343a40;\n"
"    border-radius: None;   \n"
"	/*background-color: rgba(85, 115, 155,0.1);    */\n"
"	font: 14px \"Trebuchet MS\";\n"
"}")
        self.gridLayout_3 = QGridLayout(self.tab_import)
        self.gridLayout_3.setObjectName(u"gridLayout_3")
        self.frame_4 = QFrame(self.tab_import)
        self.frame_4.setObjectName(u"frame_4")
        self.frame_4.setStyleSheet(u"")
        self.frame_4.setFrameShape(QFrame.StyledPanel)
        self.frame_4.setFrameShadow(QFrame.Raised)
        self.gridLayout_2 = QGridLayout(self.frame_4)
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.btn_importar = QPushButton(self.frame_4)
        self.btn_importar.setObjectName(u"btn_importar")
        self.btn_importar.setMinimumSize(QSize(135, 35))
        self.btn_importar.setMaximumSize(QSize(135, 35))
        self.btn_importar.setStyleSheet(u"QPushButton{\n"
"	background-color:#28a745;\n"
"    border-color: #5cb85c;\n"
"    border-radius:None;\n"
"	color: #f8f9fa;\n"
"	font: 11pt \"Segoe UI\";\n"
"}\n"
"\n"
"QPushButton:hover{\n"
"	border-color: #4cae4c;\n"
"	background-color: #5cb85c;\n"
"}\n"
"")

        self.gridLayout_2.addWidget(self.btn_importar, 1, 0, 1, 1)

        self.btn_abrir_dir = QPushButton(self.frame_4)
        self.btn_abrir_dir.setObjectName(u"btn_abrir_dir")
        self.btn_abrir_dir.setMinimumSize(QSize(135, 35))
        self.btn_abrir_dir.setMaximumSize(QSize(135, 35))
        self.btn_abrir_dir.setStyleSheet(u"QPushButton{\n"
"	background-color:#007bff;\n"
"    border-color: #286090;\n"
"    border-radius:None;\n"
"	color: #f8f9fa;\n"
"	font: 11pt \"Segoe UI\";\n"
"}\n"
"\n"
"QPushButton:hover{\n"
"	border-color: #007bff;\n"
"	background-color: #286090;\n"
"}\n"
"")

        self.gridLayout_2.addWidget(self.btn_abrir_dir, 0, 0, 1, 1)

        self.input_file = QLineEdit(self.frame_4)
        self.input_file.setObjectName(u"input_file")
        self.input_file.setMinimumSize(QSize(0, 30))
        self.input_file.setStyleSheet(u"QLineEdit{	\n"
"	color: rgb(52, 58, 64);\n"
"	border-bottom: 1px solid #343a40;\n"
"    border-radius: None;   \n"
"	background-color: rgba(85, 115, 155,0.1);\n"
"	font: 14px \"Trebuchet MS\";\n"
"}")

        self.gridLayout_2.addWidget(self.input_file, 0, 1, 1, 3)

        self.pb_carga = QProgressBar(self.frame_4)
        self.pb_carga.setObjectName(u"pb_carga")
        self.pb_carga.setStyleSheet(u"")
        self.pb_carga.setValue(0)

        self.gridLayout_2.addWidget(self.pb_carga, 1, 1, 1, 2)

        self.btn_parar = QPushButton(self.frame_4)
        self.btn_parar.setObjectName(u"btn_parar")
        self.btn_parar.setMinimumSize(QSize(80, 35))
        self.btn_parar.setMaximumSize(QSize(80, 35))
        self.btn_parar.setStyleSheet(u"QPushButton{\n"
"	background-color:#dc3545;\n"
"    border-color: #d43f3a;\n"
"    border-radius:None;\n"
"	color: #f8f9fa;\n"
"	font: 11pt \"Segoe UI\";\n"
"}\n"
"\n"
"QPushButton:hover{\n"
"	border-color: #d43f3a;\n"
"	background-color: #d9534f;\n"
"}\n"
"")

        self.gridLayout_2.addWidget(self.btn_parar, 1, 3, 1, 1)


        self.gridLayout_3.addWidget(self.frame_4, 0, 0, 1, 2)

        self.line_2 = QFrame(self.tab_import)
        self.line_2.setObjectName(u"line_2")
        self.line_2.setFrameShape(QFrame.HLine)
        self.line_2.setFrameShadow(QFrame.Sunken)

        self.gridLayout_3.addWidget(self.line_2, 1, 0, 1, 2)

        self.label_8 = QLabel(self.tab_import)
        self.label_8.setObjectName(u"label_8")
        self.label_8.setMinimumSize(QSize(0, 30))
        self.label_8.setMaximumSize(QSize(195, 30))
        self.label_8.setFont(font)

        self.gridLayout_3.addWidget(self.label_8, 2, 0, 1, 1)

        self.input_tempo_carga = QLineEdit(self.tab_import)
        self.input_tempo_carga.setObjectName(u"input_tempo_carga")
        self.input_tempo_carga.setEnabled(True)
        self.input_tempo_carga.setMinimumSize(QSize(0, 30))
        self.input_tempo_carga.setMaximumSize(QSize(16777215, 30))
        self.input_tempo_carga.setStyleSheet(u"QLineEdit{	\n"
"	color: rgb(52, 58, 64);\n"
"	border-bottom: 1px solid #343a40;\n"
"    border-radius: None;   \n"
"	background-color: rgba(85, 115, 155,0.1);\n"
"	font: 14px \"Trebuchet MS\";\n"
"}")

        self.gridLayout_3.addWidget(self.input_tempo_carga, 2, 1, 1, 1)

        self.lw_arquivos = QListWidget(self.tab_import)
        self.lw_arquivos.setObjectName(u"lw_arquivos")
        self.lw_arquivos.setStyleSheet(u"color: rgb(52, 58, 64);\n"
"font: 14px \"Trebuchet MS\";\n"
"border: 1px solid #343a40;\n"
"background-color: rgb(255, 255, 255);\n"
"/*\n"
"border: 0 0 0 0;\n"
"border-bottom: 1px solid #343a40;\n"
"\n"
"rgb(255, 255, 255);\n"
"border-bottom: 1px solid #343a40;\n"
"border-radius: None;\n"
"border-bottom: 1px solid #343a40;\n"
"background-color: rgba(85, 115, 155,0.1);    \n"
"*/")

        self.gridLayout_3.addWidget(self.lw_arquivos, 3, 0, 1, 2)

        self.tab_base.addTab(self.tab_import, "")

        self.gridLayout.addWidget(self.tab_base, 0, 0, 1, 1)

        self.Pages.addWidget(self.pg_table)
        self.pg_cadastro = QWidget()
        self.pg_cadastro.setObjectName(u"pg_cadastro")
        self.pg_cadastro.setStyleSheet(u"QLineEdit{	\n"
"	color: rgb(52, 58, 64);\n"
"	border-bottom: 1px solid #343a40;\n"
"    border-radius: None;   \n"
"	background-color: rgba(85, 115, 155,0.1);\n"
"	font: 14px \"Trebuchet MS\";\n"
"    height: 30;\n"
"}\n"
"\n"
"QLabel{\n"
"  height: 30;\n"
"  font: 14px \"Trebuchet MS\";\n"
"  width: 50;  \n"
"}\n"
"\n"
"\n"
"QComboBox {\n"
"    border: 1px solid gray;\n"
"    border-radius: 3px;\n"
"    padding: 1px 18px 1px 3px;\n"
"    min-width: 6em;\n"
"    height: 30;\n"
"    border-radius: None;\n"
"}\n"
"\n"
"\n"
"QComboBox:editable {\n"
"    background: white;\n"
"}\n"
"\n"
"/*\n"
"https://icons8.com/icons/set/arrow-down-right-16x16\n"
"\n"
"QComboBox:!editable, QComboBox::drop-down:editable {\n"
"     background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                 stop: 0 #E1E1E1, stop: 0.4 #DDDDDD,\n"
"                                 stop: 0.5 #D8D8D8, stop: 1.0 #D3D3D3);\n"
"}\n"
"*/\n"
"\n"
"QComboBox:on { /* shift the text when the popup opens */\n"
"    padding-top:"
                        " 3px;\n"
"    padding-left: 4px;\n"
"}\n"
"\n"
"QComboBox::drop-down {\n"
"    subcontrol-origin: padding;\n"
"    subcontrol-position: top right;\n"
"    width: 15px;\n"
"\n"
"    border-left-width: 1px;\n"
"    border-left-color: darkgray;\n"
"    border-left-style: solid; /* just a single line */\n"
"    border-top-right-radius: 3px; /* same radius as the QComboBox */\n"
"    border-bottom-right-radius: 3px;\n"
"}\n"
"\n"
"QComboBox::down-arrow {\n"
"    image: url(src/resources/img/icons8-drop-down-16.png);\n"
"    /*image: url(src/resources/img/icons8-drop-16.png);*/\n"
"}\n"
"\n"
"QComboBox::down-arrow:on { /* shift the arrow when popup is open */\n"
"    top: 1px;\n"
"    left: 1px;\n"
"}")
        self.verticalLayout_4 = QVBoxLayout(self.pg_cadastro)
        self.verticalLayout_4.setObjectName(u"verticalLayout_4")
        self.label_18 = QLabel(self.pg_cadastro)
        self.label_18.setObjectName(u"label_18")
        font2 = QFont()
        font2.setFamilies([u"Trebuchet MS"])
        font2.setBold(False)
        font2.setItalic(False)
        self.label_18.setFont(font2)
        self.label_18.setAlignment(Qt.AlignCenter)

        self.verticalLayout_4.addWidget(self.label_18)

        self.label_10 = QLabel(self.pg_cadastro)
        self.label_10.setObjectName(u"label_10")
        self.label_10.setFont(font2)
        self.label_10.setAlignment(Qt.AlignCenter)

        self.verticalLayout_4.addWidget(self.label_10)

        self.horizontalLayout_8 = QHBoxLayout()
        self.horizontalLayout_8.setObjectName(u"horizontalLayout_8")
        self.label_11 = QLabel(self.pg_cadastro)
        self.label_11.setObjectName(u"label_11")
        self.label_11.setMinimumSize(QSize(100, 0))
        self.label_11.setMaximumSize(QSize(100, 16777215))

        self.horizontalLayout_8.addWidget(self.label_11)

        self.input_nome = QLineEdit(self.pg_cadastro)
        self.input_nome.setObjectName(u"input_nome")

        self.horizontalLayout_8.addWidget(self.input_nome)


        self.verticalLayout_4.addLayout(self.horizontalLayout_8)

        self.horizontalLayout_7 = QHBoxLayout()
        self.horizontalLayout_7.setObjectName(u"horizontalLayout_7")
        self.label_12 = QLabel(self.pg_cadastro)
        self.label_12.setObjectName(u"label_12")
        self.label_12.setMinimumSize(QSize(100, 0))
        self.label_12.setMaximumSize(QSize(100, 16777215))

        self.horizontalLayout_7.addWidget(self.label_12)

        self.input_user = QLineEdit(self.pg_cadastro)
        self.input_user.setObjectName(u"input_user")

        self.horizontalLayout_7.addWidget(self.input_user)


        self.verticalLayout_4.addLayout(self.horizontalLayout_7)

        self.horizontalLayout_5 = QHBoxLayout()
        self.horizontalLayout_5.setObjectName(u"horizontalLayout_5")
        self.label_13 = QLabel(self.pg_cadastro)
        self.label_13.setObjectName(u"label_13")
        self.label_13.setMinimumSize(QSize(100, 0))
        self.label_13.setMaximumSize(QSize(100, 16777215))

        self.horizontalLayout_5.addWidget(self.label_13)

        self.input_senha = QLineEdit(self.pg_cadastro)
        self.input_senha.setObjectName(u"input_senha")
        self.input_senha.setEchoMode(QLineEdit.Password)

        self.horizontalLayout_5.addWidget(self.input_senha)


        self.verticalLayout_4.addLayout(self.horizontalLayout_5)

        self.horizontalLayout_4 = QHBoxLayout()
        self.horizontalLayout_4.setObjectName(u"horizontalLayout_4")
        self.label_14 = QLabel(self.pg_cadastro)
        self.label_14.setObjectName(u"label_14")
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_14.sizePolicy().hasHeightForWidth())
        self.label_14.setSizePolicy(sizePolicy)
        self.label_14.setMinimumSize(QSize(100, 0))
        self.label_14.setMaximumSize(QSize(100, 16777215))

        self.horizontalLayout_4.addWidget(self.label_14)

        self.input_senha_2 = QLineEdit(self.pg_cadastro)
        self.input_senha_2.setObjectName(u"input_senha_2")
        self.input_senha_2.setEchoMode(QLineEdit.Password)

        self.horizontalLayout_4.addWidget(self.input_senha_2)


        self.verticalLayout_4.addLayout(self.horizontalLayout_4)

        self.horizontalLayout_9 = QHBoxLayout()
        self.horizontalLayout_9.setObjectName(u"horizontalLayout_9")
        self.label_15 = QLabel(self.pg_cadastro)
        self.label_15.setObjectName(u"label_15")
        self.label_15.setMinimumSize(QSize(100, 0))
        self.label_15.setMaximumSize(QSize(100, 16777215))

        self.horizontalLayout_9.addWidget(self.label_15)

        self.cb_perfil = QComboBox(self.pg_cadastro)
        self.cb_perfil.addItem("")
        self.cb_perfil.addItem("")
        self.cb_perfil.setObjectName(u"cb_perfil")
        sizePolicy1 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.cb_perfil.sizePolicy().hasHeightForWidth())
        self.cb_perfil.setSizePolicy(sizePolicy1)

        self.horizontalLayout_9.addWidget(self.cb_perfil)


        self.verticalLayout_4.addLayout(self.horizontalLayout_9)

        self.horizontalLayout_10 = QHBoxLayout()
        self.horizontalLayout_10.setObjectName(u"horizontalLayout_10")
        self.label_17 = QLabel(self.pg_cadastro)
        self.label_17.setObjectName(u"label_17")

        self.horizontalLayout_10.addWidget(self.label_17)

        self.bt_cadastrar = QPushButton(self.pg_cadastro)
        self.bt_cadastrar.setObjectName(u"bt_cadastrar")
        self.bt_cadastrar.setStyleSheet(u"QPushButton{\n"
"	background-color:#28a745;\n"
"    border-color: #5cb85c;\n"
"    border-radius:None;\n"
"	color: #f8f9fa;\n"
"    height: 35;\n"
"	font: 11pt \"Segoe UI\";\n"
"}\n"
"\n"
"QPushButton:hover{\n"
"	border-color: #4cae4c;\n"
"	background-color: #5cb85c;\n"
"}\n"
"")

        self.horizontalLayout_10.addWidget(self.bt_cadastrar)

        self.label_16 = QLabel(self.pg_cadastro)
        self.label_16.setObjectName(u"label_16")

        self.horizontalLayout_10.addWidget(self.label_16)


        self.verticalLayout_4.addLayout(self.horizontalLayout_10)

        self.Pages.addWidget(self.pg_cadastro)
        self.pg_home = QWidget()
        self.pg_home.setObjectName(u"pg_home")
        self.verticalLayout = QVBoxLayout(self.pg_home)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.label = QLabel(self.pg_home)
        self.label.setObjectName(u"label")
        self.label.setStyleSheet(u"color: #383d41;\n"
"background-color: rgb(255, 255, 255);")

        self.verticalLayout.addWidget(self.label)

        self.Pages.addWidget(self.pg_home)
        self.pg_nsn = QWidget()
        self.pg_nsn.setObjectName(u"pg_nsn")
        self.label_5 = QLabel(self.pg_nsn)
        self.label_5.setObjectName(u"label_5")
        self.label_5.setGeometry(QRect(250, 90, 241, 41))
        font3 = QFont()
        font3.setPointSize(20)
        self.label_5.setFont(font3)
        self.Pages.addWidget(self.pg_nsn)
        self.pg_contato = QWidget()
        self.pg_contato.setObjectName(u"pg_contato")
        self.label_4 = QLabel(self.pg_contato)
        self.label_4.setObjectName(u"label_4")
        self.label_4.setGeometry(QRect(250, 70, 281, 41))
        self.label_4.setFont(font3)
        self.Pages.addWidget(self.pg_contato)
        self.pg_sobre = QWidget()
        self.pg_sobre.setObjectName(u"pg_sobre")
        self.label_3 = QLabel(self.pg_sobre)
        self.label_3.setObjectName(u"label_3")
        self.label_3.setGeometry(QRect(250, 70, 241, 41))
        self.label_3.setFont(font3)
        self.Pages.addWidget(self.pg_sobre)

        self.verticalLayout_2.addWidget(self.Pages)

        self.horizontalLayout_3 = QHBoxLayout()
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.label_6 = QLabel(self.centralwidget)
        self.label_6.setObjectName(u"label_6")

        self.horizontalLayout_3.addWidget(self.label_6)

        self.btn_pg_cadastro = QPushButton(self.centralwidget)
        self.btn_pg_cadastro.setObjectName(u"btn_pg_cadastro")
        self.btn_pg_cadastro.setMinimumSize(QSize(0, 35))
        self.btn_pg_cadastro.setCursor(QCursor(Qt.PointingHandCursor))
        self.btn_pg_cadastro.setStyleSheet(u"QPushButton{\n"
"	background-color:#343a40;\n"
"    border-color: #6c757d;\n"
"    border-radius:None;\n"
"	color: #f8f9fa;\n"
"	font: 11pt \"Segoe UI\";\n"
"}\n"
"\n"
"QPushButton:hover{\n"
"	border-color: #204d74;\n"
"	background-color: #6c757d;\n"
"}\n"
"")

        self.horizontalLayout_3.addWidget(self.btn_pg_cadastro)

        self.label_9 = QLabel(self.centralwidget)
        self.label_9.setObjectName(u"label_9")

        self.horizontalLayout_3.addWidget(self.label_9)


        self.verticalLayout_2.addLayout(self.horizontalLayout_3)

        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)

        self.Pages.setCurrentIndex(1)
        self.tab_base.setCurrentIndex(1)


        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"MainWindow", None))
        self.btn_home.setText(QCoreApplication.translate("MainWindow", u"HOME", None))
        self.btn_ncage.setText(QCoreApplication.translate("MainWindow", u"NCAGE", None))
        self.btn_nsn.setText(QCoreApplication.translate("MainWindow", u"NSN", None))
        self.btn_contato.setText(QCoreApplication.translate("MainWindow", u"CONTATO", None))
        self.btn_sobre.setText(QCoreApplication.translate("MainWindow", u"SOBRE", None))
        self.label_2.setText(QCoreApplication.translate("MainWindow", u"Filtrar Dados Tabela:", None))
        self.input_filtrar_ncage.setPlaceholderText(QCoreApplication.translate("MainWindow", u"NCAGE / Name", None))
        self.input_buscar_ncage.setPlaceholderText(QCoreApplication.translate("MainWindow", u"NCAGE Code", None))
        self.btn_pesquisar_ncage.setText(QCoreApplication.translate("MainWindow", u"Pesquisar...", None))
        self.btn_carregar.setText(QCoreApplication.translate("MainWindow", u"Carregar Tabela", None))
        self.label_7.setText("")
        self.cb_linhas_pagina.setItemText(0, QCoreApplication.translate("MainWindow", u"Linhas na P\u00e1gina", None))
        self.cb_linhas_pagina.setItemText(1, QCoreApplication.translate("MainWindow", u"100", None))
        self.cb_linhas_pagina.setItemText(2, QCoreApplication.translate("MainWindow", u"500", None))
        self.cb_linhas_pagina.setItemText(3, QCoreApplication.translate("MainWindow", u"1000", None))
        self.cb_linhas_pagina.setItemText(4, QCoreApplication.translate("MainWindow", u"5000", None))
        self.cb_linhas_pagina.setItemText(5, QCoreApplication.translate("MainWindow", u"10000", None))

        self.cb_linhas_pagina.setCurrentText(QCoreApplication.translate("MainWindow", u"Linhas na P\u00e1gina", None))
        ___qtreewidgetitem = self.tw_ncage.headerItem()
        ___qtreewidgetitem.setText(29, QCoreApplication.translate("MainWindow", u"Replacements", None));
        ___qtreewidgetitem.setText(28, QCoreApplication.translate("MainWindow", u"GLN", None));
        ___qtreewidgetitem.setText(27, QCoreApplication.translate("MainWindow", u"UNSPSC", None));
        ___qtreewidgetitem.setText(26, QCoreApplication.translate("MainWindow", u"CPV", None));
        ___qtreewidgetitem.setText(25, QCoreApplication.translate("MainWindow", u"DUNS", None));
        ___qtreewidgetitem.setText(24, QCoreApplication.translate("MainWindow", u"NACE", None));
        ___qtreewidgetitem.setText(23, QCoreApplication.translate("MainWindow", u"NAICS", None));
        ___qtreewidgetitem.setText(22, QCoreApplication.translate("MainWindow", u"ISIC", None));
        ___qtreewidgetitem.setText(21, QCoreApplication.translate("MainWindow", u"National Identification", None));
        ___qtreewidgetitem.setText(20, QCoreApplication.translate("MainWindow", u"Websites", None));
        ___qtreewidgetitem.setText(19, QCoreApplication.translate("MainWindow", u"Emails", None));
        ___qtreewidgetitem.setText(18, QCoreApplication.translate("MainWindow", u"Faxes", None));
        ___qtreewidgetitem.setText(17, QCoreApplication.translate("MainWindow", u"Telephones", None));
        ___qtreewidgetitem.setText(16, QCoreApplication.translate("MainWindow", u"Postal Address City", None));
        ___qtreewidgetitem.setText(15, QCoreApplication.translate("MainWindow", u"Postal Code", None));
        ___qtreewidgetitem.setText(14, QCoreApplication.translate("MainWindow", u"Post Office", None));
        ___qtreewidgetitem.setText(13, QCoreApplication.translate("MainWindow", u"Address City", None));
        ___qtreewidgetitem.setText(12, QCoreApplication.translate("MainWindow", u"Postal Zone", None));
        ___qtreewidgetitem.setText(11, QCoreApplication.translate("MainWindow", u"Street Address 2", None));
        ___qtreewidgetitem.setText(10, QCoreApplication.translate("MainWindow", u"Street Address 1", None));
        ___qtreewidgetitem.setText(9, QCoreApplication.translate("MainWindow", u"Province", None));
        ___qtreewidgetitem.setText(8, QCoreApplication.translate("MainWindow", u"US State", None));
        ___qtreewidgetitem.setText(7, QCoreApplication.translate("MainWindow", u"Country Code", None));
        ___qtreewidgetitem.setText(6, QCoreApplication.translate("MainWindow", u"NCAGE Type Code", None));
        ___qtreewidgetitem.setText(5, QCoreApplication.translate("MainWindow", u"Designator Code", None));
        ___qtreewidgetitem.setText(4, QCoreApplication.translate("MainWindow", u"Status", None));
        ___qtreewidgetitem.setText(3, QCoreApplication.translate("MainWindow", u"Name", None));
        ___qtreewidgetitem.setText(2, QCoreApplication.translate("MainWindow", u"Date Last Change", None));
        ___qtreewidgetitem.setText(1, QCoreApplication.translate("MainWindow", u"Date NCAGE", None));
        self.tab_base.setTabText(self.tab_base.indexOf(self.tab_ncage), QCoreApplication.translate("MainWindow", u"Base", None))
        self.btn_importar.setText(QCoreApplication.translate("MainWindow", u"Importar", None))
        self.btn_abrir_dir.setText(QCoreApplication.translate("MainWindow", u"Abrir", None))
        self.input_file.setPlaceholderText(QCoreApplication.translate("MainWindow", u"<-- Selecione o diret\u00f3rio com os arquivos XML", None))
        self.btn_parar.setText(QCoreApplication.translate("MainWindow", u"Parar", None))
        self.label_8.setText(QCoreApplication.translate("MainWindow", u"Tempo de Processamento", None))
        self.tab_base.setTabText(self.tab_base.indexOf(self.tab_import), QCoreApplication.translate("MainWindow", u"Importar XML", None))
        self.label_18.setText(QCoreApplication.translate("MainWindow", u"TELA DE CADASTRO", None))
        self.label_10.setText(QCoreApplication.translate("MainWindow", u"CADASTRAR USU\u00c1RIO", None))
        self.label_11.setText(QCoreApplication.translate("MainWindow", u"Nome:", None))
        self.label_12.setText(QCoreApplication.translate("MainWindow", u"Usu\u00e1rio:", None))
        self.label_13.setText(QCoreApplication.translate("MainWindow", u"Senha:", None))
        self.label_14.setText(QCoreApplication.translate("MainWindow", u"Confirmar:", None))
        self.label_15.setText(QCoreApplication.translate("MainWindow", u"Perfil:", None))
        self.cb_perfil.setItemText(0, QCoreApplication.translate("MainWindow", u"Usu\u00e1rio", None))
        self.cb_perfil.setItemText(1, QCoreApplication.translate("MainWindow", u"Administrador", None))

        self.label_17.setText("")
        self.bt_cadastrar.setText(QCoreApplication.translate("MainWindow", u"Cadastrar", None))
        self.label_16.setText("")
        self.label.setText(QCoreApplication.translate("MainWindow", u"<html><head/><body><p align=\"center\"><img src=\"src/resources/img/dabm.png\"/></p><p align=\"center\"><span style=\" font-size:48pt;\">NMCRL</span></p><p align=\"center\"><span style=\" font-size:36pt;\">Carga de Dados XML</span></p></body></html>", None))
        self.label_5.setText(QCoreApplication.translate("MainWindow", u"TELA DE NSNs", None))
        self.label_4.setText(QCoreApplication.translate("MainWindow", u"TELA DE CONTATO", None))
        self.label_3.setText(QCoreApplication.translate("MainWindow", u"TELA DE SOBRE", None))
        self.label_6.setText("")
        self.btn_pg_cadastro.setText(QCoreApplication.translate("MainWindow", u"CADASTRO USU\u00c1RIO", None))
        self.label_9.setText("")
    # retranslateUi

