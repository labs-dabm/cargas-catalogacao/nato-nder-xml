DROP TABLE FEDLOGDB.SUPPLIER CASCADE CONSTRAINTS;

CREATE TABLE FEDLOGDB.SUPPLIER
(
  CAGE_CODE              CHAR(5 BYTE)           NOT NULL,
  CAGE_STATUS            CHAR(1 BYTE),
  CAGE_TYPE              CHAR(1 BYTE),
  CAGE_CAO               CHAR(6 BYTE),
  CAGE_ADP               CHAR(6 BYTE),
  CAGE_RPLM              CHAR(5 BYTE),
  CAGE_ASSOC             CHAR(5 BYTE),
  CAGE_AFFIL             CHAR(1 BYTE),
  CAGE_SIZE              CHAR(1 BYTE),
  CAGE_PRIMARY_BUSINESS  CHAR(1 BYTE),
  CAGE_TYPE_OF_BUSINESS  CHAR(1 BYTE),
  CAGE_WOMAN_OWNED       CHAR(1 BYTE),
  CAGE_SIC_CODES         VARCHAR2(40 BYTE),
  CAGE_FAX               VARCHAR2(40 BYTE),
  CAGE_COMPANY_NAME      VARCHAR2(300 BYTE),
  CAGE_COMPANY_ADDRESS   VARCHAR2(300 BYTE),
  CAGE_PO_BOX            VARCHAR2(80 BYTE),
  CAGE_CITY              VARCHAR2(200 BYTE),
  CAGE_STATE             VARCHAR2(100 BYTE),
  CAGE_COUNTRY           VARCHAR2(200 BYTE),
  CAGE_ZIP_CODE          VARCHAR2(30 BYTE),
  CAGE_TELEPHONE         VARCHAR2(30 BYTE),
  CAGE_FORMER_ADDRESS    VARCHAR2(200 BYTE)
);


ALTER TABLE FEDLOGDB.SUPPLIER ADD (
  PRIMARY KEY
  (CAGE_CODE)
  ENABLE VALIDATE);


CREATE INDEX FEDLOGDB.SUPPLIER_CAGE_COMPANY_IDX ON FEDLOGDB.SUPPLIER
(CAGE_COMPANY_NAME);

--  There is no statement for index FEDLOGDB.SYS_C007749.
--  The object is created when the parent object is created.

CREATE OR REPLACE PUBLIC SYNONYM SUPPLIER FOR FEDLOGDB.SUPPLIER;
