SELECT KHN.NCAGE,
       KHN.KUSFDDC,
       KHN.KNORG,
       KHN.BOX,
       KHN.OBEORG,
       KHN.KSTUSCAN,
       KHN.KSTJINY,
       KHN.KPCC,
       KHN.PSC,
       KHN.KPCS,
       KHN.TLX,
       KHN.VTX,
       KHN.PSS,
       KHN.INF,
       KHN.KNSCMSD,
       KHN.KTOENT,
       KHN.KIDN,
       KHN.KISC,
       KHN.KDZMKHN,
       KHN.KMOZ,
       --(SELECT P.ISO FROM MCCPROD.KCZEME P WHERE P.KZEME = KHN.KMOZ) DRN_3408,
       KHN.FOOTPRINT,
       KHN.VERSION_ID,
       KHN.STREET1,
       KHN.STREET2,
       KHN.KTIMESTAMP,
       KHN.DRN_2262,
       KHN.DUNS,
       KHN.KPRMSR
  FROM FEDLOGDB.NMCRL_ORG  KHN
 WHERE 1 = 1 
   --AND KHN.NCAGE = 'D8046'
   AND KHN.NCAGE IN (
     '002LU'
   --, '002QK'
   --,'002SU'
   )
 ;