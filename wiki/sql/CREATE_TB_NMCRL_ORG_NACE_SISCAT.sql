--DROP TABLE FEDLOGDB.NMCRL_ORG_NACE CASCADE CONSTRAINTS;

CREATE TABLE FEDLOGDB.NMCRL_ORG_NACE
(
  NCAGE  VARCHAR2(5 BYTE)                       NOT NULL,
  NACE   VARCHAR2(50 CHAR)                      NOT NULL
);


CREATE UNIQUE INDEX FEDLOGDB.P_NMCRL_ORG_NACE ON FEDLOGDB.NMCRL_ORG_NACE
(NCAGE, NACE);

ALTER TABLE FEDLOGDB.NMCRL_ORG_NACE ADD (
  CONSTRAINT P_NMCRL_ORG_NACE
  PRIMARY KEY
  (NCAGE, NACE)
  USING INDEX FEDLOGDB.P_NMCRL_ORG_NACE
  ENABLE VALIDATE);


CREATE INDEX FEDLOGDB.I_NMCRL_ORG_NACE_NCAGE ON FEDLOGDB.NMCRL_ORG_NACE
(NCAGE);

ALTER TABLE FEDLOGDB.NMCRL_ORG_NACE ADD (
  CONSTRAINT F_NMCRLORGNACE_NMCRLORG 
  FOREIGN KEY (NCAGE) 
  REFERENCES FEDLOGDB.NMCRL_ORG (NCAGE)
  ENABLE VALIDATE);
