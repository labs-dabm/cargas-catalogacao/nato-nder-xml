--DROP TABLE FEDLOGDB.NMCRL_ORG_FAX CASCADE CONSTRAINTS;

CREATE TABLE FEDLOGDB.NMCRL_ORG_FAX
(
  NCAGE  VARCHAR2(5 BYTE)                       NOT NULL,
  FAX    VARCHAR2(50 CHAR)                      NOT NULL
)
;

CREATE UNIQUE INDEX FEDLOGDB.P_NMCRL_ORG_FAX ON FEDLOGDB.NMCRL_ORG_FAX (NCAGE, FAX);

CREATE INDEX FEDLOGDB.I_NMCRL_ORG_FAX_NCAGE ON FEDLOGDB.NMCRL_ORG_FAX (NCAGE);

ALTER TABLE FEDLOGDB.NMCRL_ORG_FAX ADD (
  CONSTRAINT P_NMCRL_ORG_FAX
  PRIMARY KEY
  (NCAGE, FAX)
  USING INDEX FEDLOGDB.P_NMCRL_ORG_FAX
  ENABLE VALIDATE);
  
ALTER TABLE FEDLOGDB.NMCRL_ORG_FAX ADD (
  CONSTRAINT F_NMCRLORGFAX_NMCRLORG 
  FOREIGN KEY (NCAGE) 
  REFERENCES FEDLOGDB.NMCRL_ORG (NCAGE)
  ENABLE VALIDATE);  