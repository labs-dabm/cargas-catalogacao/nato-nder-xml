# app.py
import os
from os.path import join, dirname, abspath
from dotenv import load_dotenv
from src.data.database_oracle import DatabaseOracle
from src.models.NcageXMLRead import NcageXMLRead

dotenv_path = join(dirname(__file__), '.env')
load_dotenv(dotenv_path)

# ORA_USERNAME = os.environ.get("ORA_USERNAME")
# ORA_DSN = os.environ.get("ORA_DSN")

# print(ORA_USERNAME)
# print(ORA_DSN)

SRC_DIR = join(abspath('.'), 'src')  # dir de codigo
FILE_DIR = dirname(abspath(__file__))  # dir de codigo
BASE_DIR = dirname(SRC_DIR)  # dir do projeto

db = DatabaseOracle()
db.version()

# print(f'dir do src projeto: {SRC_DIR}')
# print(f'filedir de codigo: {FILE_DIR}')
# print(f'dir do projeto: {BASE_DIR}')

# pycharm
# xml = NcageXMLRead('../../wiki/xml/extrato_ncage')
# xml = NcageXMLRead('../../wiki/xml/ncage')
# vscode code runner
# xml = NcageXMLRead('wiki\\xml\\extrato_ncage')
xml = NcageXMLRead('wiki\\xml\\ncage')
# ler todos os arquivos
lista_xml = xml.all_files()

# print(lista_xml)
print(len(lista_xml))
all_items = xml.xml_data(lista_xml[0])

print('NCAGE', len(all_items))

sql = """ ALTER TABLE FEDLOGDB.NMCRL_ORG_REPL
          DISABLE CONSTRAINT F_NMCRLORGREPL_NMCRLORG_REPL """
db.execute(sql)


# print(all_items)
db.insert_ncage_org(all_items)

# # INSERTs DAS LISTAS
r1 = NcageXMLRead.monta_tupla_ncage(xml.ls_drn_8974)
r2 = NcageXMLRead.monta_tupla_ncage(xml.ls_drn_8975)
r3 = NcageXMLRead.monta_tupla_ncage(xml.ls_drn_3375)
r4 = NcageXMLRead.monta_tupla_ncage(xml.ls_drn_8021)
r5 = NcageXMLRead.monta_tupla_ncage(xml.ls_drn_1368)
r6 = NcageXMLRead.monta_tupla_ncage(xml.ls_drn_6044)
r7 = NcageXMLRead.monta_tupla_ncage(xml.ls_drn_2657)
r8 = NcageXMLRead.monta_tupla_ncage(xml.ls_drn_9569)
r9 = NcageXMLRead.monta_tupla_ncage(xml.ls_drn_9574)
r10 = NcageXMLRead.monta_tupla_ncage(xml.ls_drn_9568)
r11 = NcageXMLRead.monta_tupla_ncage(xml.ls_drn_3595)
# r11 = xml.ls_drn_3595

db.insert_ncage_dados(r1, 'TEL')
db.insert_ncage_dados(r2, 'FAX')
db.insert_ncage_dados(r3, 'MAIL')
db.insert_ncage_dados(r4, 'WEB')
db.insert_ncage_dados(r5, 'SICC')
db.insert_ncage_dados(r6, 'NAICS')
db.insert_ncage_dados(r7, 'NACE')
db.insert_ncage_dados(r8, 'CPV')
db.insert_ncage_dados(r9, 'UNSPSC')
db.insert_ncage_dados(r10, 'GLN')
db.insert_ncage_dados(r11, 'REPL')

print('TEL', len(r1))
print('FAX', len(r2))
print('MAIL', len(r3))
print('WEB', len(r4))
print('SICC', len(r5))
print('AICS', len(r6))
print('NACE', len(r7))
print('CPV', len(r8))
print('SPSC', len(r9))
print('GLN', len(r10))
print('REPL', len(r11))
# print(r11)


# s1 = set()

# s1.add('casa')
# s1.add('casa')
# s1.add('alberto')
# s1.add('alberto')
# s1.add('silva')
# s1.add('silva')
# s1.add('anders')

# print('SET:', s1)
# print('TUPLE:', tuple(s1))
# print('LIST:', list(s1))
# tb_column = 'REPL'
# param2 = tb_column
# if tb_column == 'REPL':
#     param2 = 'REPLACEMENT'

# sql_insert = """ INSERT INTO FEDLOGDB.NMCRL_ORG_ % s
#                     (NCAGE, %s)
#                     values
#                     (:1,:2)
#                     """ % (tb_column, param2)

# print(sql_insert)
# data = 11000
# batch_size = 10000
# # if data % batch_size == 0:
# print(data % batch_size)
